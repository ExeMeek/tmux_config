# Custom tmux repo

This is not an official repo. I'm just hosting for my own ease of access.

Here's a breakdown of whats in store:
- plugins from https://github.com/tmux-plugins/tpm
- config from me
- config backup from me
